﻿using AutoMapper;
using Project.Service.DAL;
using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Test_Project.ViewModels;

namespace Test_Project
{
 
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer(new VehicleInitializer());

            

        AutoMapper.Mapper.Initialize(config =>
            {

 
                config.CreateMap<VehicleMake, VehicleMakeVM>().ReverseMap()
                .ForMember(model => model.VehicleModels, model => model.Ignore())
                .AfterMap((src, dest) =>
                {
                    if (src.VehicleModels != null)
                    { dest.VehicleModels = src.VehicleModels; }                 
                });
                
                config.CreateMap<VehicleModel, VehicleModelVM>().ReverseMap()
                .ForMember(model => model.VehicleMake, model => model.Ignore())
                .ForMember(model => model.VehicleMakeId, model => model.Ignore())
                .AfterMap((src, dest) =>
                {
                    if (src.VehicleMake != null)
                    { dest.VehicleMake = src.VehicleMake; }
                    if (src.VehicleMakeId != Guid.Empty)
                    {dest.VehicleMakeId = src.VehicleMakeId;}
                });
            
                config.CreateMap<VehicleMake, VehicleMakeDropdownVM>()
                .ForMember(dest => dest.id, opts => opts.MapFrom(src => src.Id))
                .ForMember(dest => dest.text, opts => opts.MapFrom(src => src.Name));                 
            });
        }
    }
}
