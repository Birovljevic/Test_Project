﻿using AutoMapper;
using Project.Service.DAL;
using Project.Service.Models;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Web.Mvc;
using PagedList;
using System;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using Test_Project.ViewModels;
using Project.Service.Interfaces;
using static Project.Service.Enums;
using Project.Service.Service;
using Project.Service.Helpers;

namespace Test_Project.Controllers
{
    public class VehicleModelController : Controller
    {
        private IVehicleModelService _vehicleModelService;
        private IVehicleMakeService _vehicleMakeService;

        public VehicleModelController()
        {
            this._vehicleModelService = new VehicleModelService();
            this._vehicleMakeService = new VehicleMakeService();
        }
        // GET: VehicleModel
        [Route("VehicleModel")]
        public async Task<ActionResult> IndexAsync(string sortOrderField, SortOrderDirection? sortOrderDirection, string searchTerm, string currentFilter, int? page)
        {
            if (String.IsNullOrEmpty(sortOrderField))
            {
                sortOrderField = "name";
            }

            if (sortOrderDirection == null)
            {
                sortOrderDirection = SortOrderDirection.asc;
            }

            ViewBag.SortOrderDirection = sortOrderDirection == SortOrderDirection.asc ? SortOrderDirection.desc : SortOrderDirection.asc;

            ViewBag.CurrentSortOrderField = sortOrderField;
            ViewBag.CurrentSortOrderDirection = sortOrderDirection;

            if (searchTerm != null)
            {
                page = 1;
            }
            else
            {
                searchTerm = currentFilter;
            }
            ViewBag.CurrentFilter = searchTerm;

            Filtering filter = new Filtering { SearchTerm = searchTerm };
            Sorting sort = new Sorting { Field = sortOrderField, Direction = sortOrderDirection.GetValueOrDefault() };
            Paging pagination = new Paging { PageSize = 4, PageNumber = (page ?? 1) };

            var count = await _vehicleModelService.GetVehicleModelCountAsync(searchTerm);

            var vehicleModelsVM = Mapper.Map<List<VehicleModelVM>>(await _vehicleModelService.GetVehicleModelsAsync(filter, sort, pagination));

            return View("Index", new StaticPagedList<VehicleModelVM>(vehicleModelsVM, pagination.PageNumber, pagination.PageSize, count));
        }

        public async Task<JsonResult> GetMakesForDropdownAsync(string searchTerm, int pageSize, int pageNum)
        {
            try
            {
                Paging pagination = new Paging { PageSize = pageSize, PageNumber = pageNum };
                Sorting sort = new Sorting { Field = "name", Direction = SortOrderDirection.asc };
                Filtering filter = new Filtering { SearchTerm = searchTerm };

                var vehicleMakeList = await _vehicleMakeService.GetVehicleMakesAsync(filter, sort, pagination);
                VehicleMakeDropdownPagedVM vehicleMakeDropdownPagedVM = new VehicleMakeDropdownPagedVM()
                {
                    Count_Results = await _vehicleMakeService.GetVehicleMakeCountAsync(searchTerm),
                    Results = Mapper.Map<List<VehicleMakeDropdownVM>>(vehicleMakeList)
                };

                return Json(vehicleMakeDropdownPagedVM, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        // GET: VehicleModel/Details
        [Route("VehicleModel/Details/{Id}")]
        public async Task<ActionResult> DetailsAsync(Guid id)
        {
            var vehicleModel = await _vehicleModelService.GetVehicleModelByIdAsync(id);

            if (vehicleModel == null)
            {
                return HttpNotFound();
            }

            var vehicleModelVM = Mapper.Map<VehicleModelVM>(vehicleModel);
            return View("Details", vehicleModelVM);
        }

        // GET: VehicleModel/Create
        [Route("VehicleModel/Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: VehicleModel/Create
        [HttpPost]
        [Route("VehicleModel/Create")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync([Bind(Include = "VehicleMakeId, Name, Abrv")]VehicleModelVM vehicleModelVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var vehicleModel = Mapper.Map<VehicleModel>(vehicleModelVM);
                    bool isInsertSuccessful = await _vehicleModelService.InsertVehicleModelAsync(vehicleModel);

                    if (isInsertSuccessful)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                    }
                }
            }
            catch (RetryLimitExceededException  /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View("Create", vehicleModelVM);
        }

        // GET: VehicleModel/Edit
        [Route("VehicleModel/Edit/{Id}")]
        public async Task<ActionResult> EditAsync(Guid id)
        {
            var vehicleModel = await _vehicleModelService.GetVehicleModelByIdAsync(id);
            if (vehicleModel == null)
            {
                return HttpNotFound();
            }
            var vehicleModelVM = Mapper.Map<VehicleModelVM>(vehicleModel);
            return View("Edit", vehicleModelVM);
        }

        [HttpPost]
        [Route("VehicleModel/Edit/{Id}")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync([Bind(Include = "ID, Name, Abrv, VehicleMakeId")]VehicleModelVM vehicleModelVM)
        {
            var vehicleModel = await _vehicleModelService.GetVehicleModelByIdAsync(vehicleModelVM.ID);
            if (vehicleModel == null)
            {
                return HttpNotFound();
            }
            try
            {
                if (ModelState.IsValid)
                {
                    Mapper.Map(vehicleModelVM, vehicleModel);
                    bool isUpdateSuccessful = await _vehicleModelService.UpdateVehicleModelAsync(vehicleModel);
                    if (isUpdateSuccessful)
                    {
                        return RedirectToAction("IndexAsync");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Unable to update. Try again, and if the problem persists see your system administrator.");
                    }
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to update. Try again, and if the problem persists see your system administrator.");
            }
            return View("Edit", vehicleModelVM);
        }

        [Route("VehicleModel/Delete/{Id}")]
        public async Task<ActionResult> DeleteAsync(Guid id, bool? saveChangesError = false)
        {
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }

            var vehicleModelToDelete = await _vehicleModelService.GetVehicleModelByIdAsync(id);

            if (vehicleModelToDelete == null)
            {
                return HttpNotFound();
            }

            var vehicleModelToDeleteVM = Mapper.Map<VehicleModelVM>(vehicleModelToDelete);
            return View("Delete", vehicleModelToDeleteVM);
        }

        [HttpPost]
        [Route("VehicleModel/Delete/{Id}")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            try
            {
                bool isDeleteSuccessful = await _vehicleModelService.DeleteVehicleModelAsync(id);
                if (!isDeleteSuccessful)
                {
                    return RedirectToAction("DeleteAsync", new { id = id, saveChangesError = true });
                }
            }
            catch (DataException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("DeleteAsync", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("IndexAsync");
        }

        protected override void Dispose(bool disposing)
        {
            _vehicleModelService.Dispose();
            _vehicleMakeService.Dispose();
            base.Dispose(disposing);
        }
    }
}
