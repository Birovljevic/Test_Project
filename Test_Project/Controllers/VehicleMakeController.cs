﻿using Project.Service.DAL;
using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using System.Data;
using PagedList;
using System.Threading.Tasks;
using Test_Project.ViewModels;
using Project.Service.Interfaces;
using static Project.Service.Enums;
using Project.Service.Service;
using Project.Service.Helpers;

namespace Test_Project.Controllers
{
    public class VehicleMakeController : Controller
    {
        private IVehicleMakeService _vehicleMakeService;

        public VehicleMakeController()
        {
            this._vehicleMakeService = new VehicleMakeService();
        }
        // GET: VehicleMake    
        [Route("VehicleMake")]    
        public async Task<ActionResult> IndexAsync(string sortOrderField, SortOrderDirection? sortOrderDirection, string searchTerm, string currentFilter, int? page)
        {
           if (String.IsNullOrEmpty(sortOrderField))
            {
               sortOrderField = "name";
            }

           if (sortOrderDirection == null)
            {
               sortOrderDirection = SortOrderDirection.asc;
            }

            ViewBag.SortOrderDirection = sortOrderDirection == SortOrderDirection.asc ? SortOrderDirection.desc : SortOrderDirection.asc;

            ViewBag.CurrentSortOrderField = sortOrderField;
            ViewBag.CurrentSortOrderDirection = sortOrderDirection;

            if (searchTerm != null)
            {
                page = 1;
            }
            else
            {
                searchTerm = currentFilter;
            }
            ViewBag.CurrentFilter = searchTerm;
     
            Filtering filter = new Filtering { SearchTerm = searchTerm };
            Sorting sort = new Sorting { Field = sortOrderField, Direction = sortOrderDirection.GetValueOrDefault() };
            Paging pagination = new Paging { PageSize = 4, PageNumber = (page ?? 1) };
                       
            var count = await _vehicleMakeService.GetVehicleMakeCountAsync(searchTerm);
            var vehicleMakesVM = Mapper.Map<List<VehicleMakeVM>>(await _vehicleMakeService.GetVehicleMakesAsync(filter, sort, pagination));
            
            return View("Index", new StaticPagedList<VehicleMakeVM>(vehicleMakesVM, pagination.PageNumber, pagination.PageSize, count));
        }

        // GET: VehicleMake/Details
        [Route("VehicleMake/Details/{Id}")]
        public async Task<ActionResult> DetailsAsync(Guid id)
        {         
            var vehicleMake = await _vehicleMakeService.GetVehicleMakeByIdAsync(id);

            if (vehicleMake == null)
            {
                return HttpNotFound();
            }

            var vehicleMakeVM = Mapper.Map<VehicleMakeVM>(vehicleMake);
            return View("Details", vehicleMakeVM);
        }

        // GET: VehicleMake/Create
        [Route("VehicleMake/Create")]
        public ActionResult Create()
        {
            return View("Create");
        }

        // POST: VehicleMake/Create
        [Route("VehicleMake/Create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync([Bind(Include = "Name, Abrv")]VehicleMakeVM vehicleMakeVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var vehicleMake = Mapper.Map<VehicleMake>(vehicleMakeVM);
                    bool isInsertSuccessful = await _vehicleMakeService.InsertVehicleMakeAsync(vehicleMake);
                    if (isInsertSuccessful)
                    {
                        return RedirectToAction("IndexAsync");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
                    }              
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View("Create", vehicleMakeVM);
        }

        // GET: VehicleMake/Edit
        [HttpGet]
        [Route("VehicleMake/Edit/{Id}")]
        public async Task<ActionResult> EditAsync(Guid id)
        {  
            var vehicleMake = await _vehicleMakeService.GetVehicleMakeByIdAsync(id);
            if (vehicleMake == null)
            {
                return HttpNotFound();
            }
            var vehicleMakeVM = Mapper.Map<VehicleMakeVM>(vehicleMake);
            return View("Edit", vehicleMakeVM);
        }

        [HttpPost]
        [Route("VehicleMake/Edit/{Id}")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync([Bind(Include = "ID, Name, Abrv")]VehicleMakeVM vehicleMakeVM)
        {
            var vehicleMake = await _vehicleMakeService.GetVehicleMakeByIdAsync(vehicleMakeVM.ID);
            if (vehicleMake == null)
            {
                return HttpNotFound();
            }
            try
            {
                if (ModelState.IsValid)
                {
                    Mapper.Map(vehicleMakeVM, vehicleMake);
                    bool isUpdateSuccessful = await _vehicleMakeService.UpdateVehicleMakeAsync(vehicleMake);
                    if (isUpdateSuccessful)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Unable to update. Try again, and if the problem persists see your system administrator.");     
                    }
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to update. Try again, and if the problem persists see your system administrator.");
            }
            return View("Edit", vehicleMakeVM);
        }
        // GET: VehicleMake/Delete
        [Route("VehicleMake/Delete/{Id}")]
        public async Task<ActionResult> DeleteAsync(Guid id, bool? saveChangesError = false)
        {         
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Delete failed. Try again, and if the problem persists see your system administrator.";
            }

            var vehicleMakeToDelete = await _vehicleMakeService.GetVehicleMakeByIdAsync(id);

            if (vehicleMakeToDelete == null)
            {
                return HttpNotFound();
            }

            var vehicleMakeToDeleteVM = Mapper.Map<VehicleMakeVM>(vehicleMakeToDelete);
            return View("Delete", vehicleMakeToDeleteVM);
        }

        [HttpPost]
        [Route("VehicleMake/Delete/{Id}")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            try
            {
                bool isDeleteSuccessful = await _vehicleMakeService.DeleteVehicleMakeAsync(id);
                if (!isDeleteSuccessful)
                {
                    return RedirectToAction("DeleteAsync", new { id = id, saveChangesError = true });
                }         
            }
            catch (DataException/* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                return RedirectToAction("DeleteAsync", new { id = id, saveChangesError = true });
            }
            return RedirectToAction("IndexAsync");
        }

        protected override void Dispose(bool disposing)
        {
            _vehicleMakeService.Dispose();
            base.Dispose(disposing);
        }
    }
}