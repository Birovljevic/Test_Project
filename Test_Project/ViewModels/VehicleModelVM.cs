﻿using Project.Service.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Test_Project.ViewModels
{
    public class VehicleModelVM
    {
        public Guid ID { get; set; }
        [Required]
        public Guid VehicleMakeId { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Name must be a string with a capital first letter and maximum length of 50 characters.")]
        public string Name { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation must be a string with maximum length of 20 characters")]
        [Display(Name = "Abbreviation")]
        public string Abrv { get; set; }      
        [Display(Name = "Vehicle Make")]
        public VehicleMake VehicleMake { get; set; }       
    }
}
