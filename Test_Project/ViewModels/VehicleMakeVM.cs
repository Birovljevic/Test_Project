﻿using Project.Service.Interfaces;
using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Project.ViewModels
{
    public class VehicleMakeVM
    {
        public Guid ID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Name must be a string with maximum length of 50 characters.")]
        public string Name { get; set; }
        [Required]
        [StringLength(20, ErrorMessage = "Abbreviation must be a string with maximum length of 20 characters")]
        [Display(Name = "Abbreviation")]
        public string Abrv { get; set; }
    
        public ICollection<VehicleModel> VehicleModels { get; set; }
    }
}
