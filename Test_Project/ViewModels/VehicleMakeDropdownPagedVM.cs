﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_Project.ViewModels
{
    public class VehicleMakeDropdownPagedVM
    {
        public int Count_Results { get; set; }        
        public List<VehicleMakeDropdownVM> Results { get; set; }
    }
}
