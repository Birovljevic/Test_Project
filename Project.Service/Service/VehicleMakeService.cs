﻿using Project.Service.DAL;
using Project.Service.Interfaces;
using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Project.Service.Enums;

namespace Project.Service.Service
{
    public class VehicleMakeService : IVehicleMakeService, IDisposable
    {
        private IVehicleContext _context;

        public VehicleMakeService()
        {
            this._context = new VehicleContext();
        }

        #region VehicleMake   
        private IQueryable<VehicleMake> GetVehicleMakeQueryFiltered(string searchTerm)
        {
            var makes = from m in _context.VehicleMakes
                        select m;
            if (!String.IsNullOrEmpty(searchTerm))
            {
                makes = makes.Where(s => s.Name.Contains(searchTerm)
                                       || s.Abrv.Contains(searchTerm));
            }
            return makes;
        }
        public Task<int> GetVehicleMakeCountAsync(string searchTerm)
        {
            return GetVehicleMakeQueryFiltered(searchTerm).CountAsync();
        }
        public Task<List<VehicleMake>> GetVehicleMakesAsync(IFiltering filter, ISorting sort, IPaging pagination)
        {
            var makes = GetVehicleMakeQueryFiltered(filter.SearchTerm);

            if (sort.Field.Equals("abrv") && sort.Direction == SortOrderDirection.asc)
            {
                makes = makes.OrderBy(m => m.Abrv);
            }
            else if (sort.Field.Equals("abrv") && sort.Direction == SortOrderDirection.desc)
            {
                makes = makes.OrderByDescending(m => m.Abrv);
            }
            else if (sort.Field.Equals("name") && sort.Direction == SortOrderDirection.desc)
            {
                makes = makes.OrderByDescending(m => m.Name);
            }
            else
            {
                makes = makes.OrderBy(m => m.Name);
            }

            return makes
                .Skip(pagination.PageSize * (pagination.PageNumber - 1))
                .Take(pagination.PageSize).ToListAsync();
        }

        public Task<VehicleMake> GetVehicleMakeByIdAsync(Guid vehicleMakeId)
        {
            return _context.VehicleMakes.FindAsync(vehicleMakeId);
        }

        public async Task<bool> InsertVehicleMakeAsync(VehicleMake vehicleMake)
        {
            _context.VehicleMakes.Add(vehicleMake);
            return (await SaveAsync() > 0);
        }

        public async Task<bool> DeleteVehicleMakeAsync(Guid vehicleMakeId)
        {

            VehicleMake vehicleMake = await _context.VehicleMakes.FindAsync(vehicleMakeId);
            if (vehicleMake != null)
            {
                _context.VehicleMakes.Remove(vehicleMake);
                return (await SaveAsync() > 0);
            }
            else
            {
                return false;
            }                    
        }

        public async Task<bool> UpdateVehicleMakeAsync(VehicleMake vehicleMake)
        {
            _context.Entry(vehicleMake).State = System.Data.Entity.EntityState.Modified;          
            return (await SaveAsync() > 0);
        }
        #endregion

        public Task<int> SaveAsync()
        {                         
            return _context.SaveChangesAsync();                 
        }
     
        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
