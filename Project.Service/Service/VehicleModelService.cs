﻿using Project.Service.DAL;
using Project.Service.Interfaces;
using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Project.Service.Enums;

namespace Project.Service.Service
{
    public class VehicleModelService : IVehicleModelService, IDisposable
    {
        private IVehicleContext _context;

        public VehicleModelService()
        {
            this._context = new VehicleContext();
        }

        #region VehicleModel
        private IQueryable<VehicleModel> GetVehicleModelQueryFiltered(string searchTerm)
        {
            var models = from m in _context.VehicleModels
                         select m;
            if (!String.IsNullOrEmpty(searchTerm))
            {
                models = models.Where(s => s.VehicleMake.Name.Contains(searchTerm)
                                       || s.VehicleMake.Abrv.Contains(searchTerm));
            }
            return models;
        }
        public Task<int> GetVehicleModelCountAsync(string searchTerm)
        {
            return GetVehicleModelQueryFiltered(searchTerm).CountAsync();
        }
        public Task<List<VehicleModel>> GetVehicleModelsAsync(IFiltering filter, ISorting sort, IPaging pagination)
        {
            var models = GetVehicleModelQueryFiltered(filter.SearchTerm);

            if (sort.Field.Equals("make") && sort.Direction == SortOrderDirection.asc)
            {
                models = models.OrderBy(m => m.VehicleMake.Name);
            }
            else if (sort.Field.Equals("make") && sort.Direction == SortOrderDirection.desc)
            {
                models = models.OrderByDescending(m => m.VehicleMake.Name);
            }
            else if (sort.Field.Equals("abrv") && sort.Direction == SortOrderDirection.asc)
            {
                models = models.OrderBy(m => m.Abrv);
            }
            else if (sort.Field.Equals("abrv") && sort.Direction == SortOrderDirection.desc)
            {
                models = models.OrderByDescending(m => m.Abrv);
            }
            else if (sort.Field.Equals("name") && sort.Direction == SortOrderDirection.desc)
            {
                models = models.OrderByDescending(m => m.Name);
            }
            else
            {
                models = models.OrderBy(m => m.Name);
            }

            return models
                .Skip(pagination.PageSize * (pagination.PageNumber - 1))
                .Take(pagination.PageSize).ToListAsync();
        }

        public Task<VehicleModel> GetVehicleModelByIdAsync(Guid vehicleModelId)
        {
            return _context.VehicleModels.FindAsync(vehicleModelId);
        }

        public async Task<bool> InsertVehicleModelAsync(VehicleModel vehicleModel)
        {
            _context.VehicleModels.Add(vehicleModel);
            return (await SaveAsync() > 0);
        }

        public async Task<bool> DeleteVehicleModelAsync(Guid vehicleModelId)
        {
            VehicleModel vehicleModel = await _context.VehicleModels.FindAsync(vehicleModelId);
            if (vehicleModel != null)
            {
                _context.VehicleModels.Remove(vehicleModel);
                return (await SaveAsync() > 0);
            }
            else
            {
                return false;
            }          
        }

        public async Task<bool> UpdateVehicleModelAsync(VehicleModel vehicleModel)
        {
            _context.Entry(vehicleModel).State = System.Data.Entity.EntityState.Modified;
            return (await SaveAsync() > 0);
        }
        #endregion

        public Task<int> SaveAsync()
        {
            return _context.SaveChangesAsync();         
        }

        #region Dispose
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}

