// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.5
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning


namespace Project.Service.Configuration
{
    using Project.Service.Models;

    // VehicleMake
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.36.1.0")]
    public class VehicleMakeConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<VehicleMake>
    {
        public VehicleMakeConfiguration()
            : this("dbo")
        {
        }

        public VehicleMakeConfiguration(string schema)
        {
            ToTable("VehicleMake", schema);
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName(@"ID").HasColumnType("uniqueidentifier").IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(x => x.Name).HasColumnName(@"Name").HasColumnType("nvarchar").IsRequired().HasMaxLength(50);
            Property(x => x.Abrv).HasColumnName(@"Abrv").HasColumnType("nvarchar").IsRequired().HasMaxLength(20);
        }
    }

}
// </auto-generated>
