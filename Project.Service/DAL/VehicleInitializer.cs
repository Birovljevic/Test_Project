﻿using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.DAL
{
    //DropCreateDatabaseIfModelChanges
    public class VehicleInitializer : System.Data.Entity.DropCreateDatabaseAlways<VehicleContext>
    {
        private void insertModelsInDB(List<VehicleModel> listOfModels, VehicleContext _context)
        {
            listOfModels.ForEach(m => _context.VehicleModels.Add(m));
        }
        protected override void Seed(VehicleContext _context)
        {
            #region Vehicle Makes
            var listVehicleMakes = new List<VehicleMake>
            {
                new VehicleMake {Name = "Alfa Romeo", Abrv = "ALFA", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Alfasud", Abrv = "SUD" },
                    new VehicleModel {Name = "Alfetta", Abrv = "FETTA" },
                    new VehicleModel {Name = "Brera", Abrv = "BRRA" },
                    new VehicleModel {Name = "Cross Wagon", Abrv = "CW" },
                    new VehicleModel {Name = "Giulia", Abrv = "GLA" }
                }},
                new VehicleMake {Name = "Audi", Abrv = "AUDI", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "A3 Coupe", Abrv = "A3C" },
                    new VehicleModel {Name = "A4 Avant", Abrv = "AVNT" },
                    new VehicleModel {Name = "A5 Coupe", Abrv = "A5C" },
                    new VehicleModel {Name = "A6 Allroad", Abrv = "A6ROAD" },
                    new VehicleModel {Name = "Q7", Abrv = "Q7" }
                }},
                new VehicleMake {Name = "Bentley", Abrv = "BENT", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Arnage", Abrv = "ARNG" },
                    new VehicleModel {Name = "Azure", Abrv = "AZR" },
                    new VehicleModel {Name = "Eight", Abrv = "EGT" },
                    new VehicleModel {Name = "Mulsanne", Abrv = "MUL" },
                    new VehicleModel {Name = "Turbo R", Abrv = "TR" }
                }},
                new VehicleMake {Name = "Citroen", Abrv = "CITR", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Berlingo", Abrv = "BERL" },
                    new VehicleModel {Name = "C-Elysse", Abrv = "ELY" },
                    new VehicleModel {Name = "C3-Aircross", Abrv = "AIR" },
                    new VehicleModel {Name = "Nemo", Abrv = "NEMO" },
                    new VehicleModel {Name = "Xsara", Abrv = "XARA" }
                }},
                new VehicleMake {Name = "Dodge", Abrv = "DDG", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Avenger", Abrv = "AVE" },
                    new VehicleModel {Name = "Challenger", Abrv = "CHAL" },
                    new VehicleModel {Name = "Charger", Abrv = "CHAR" },
                    new VehicleModel {Name = "Magnum", Abrv = "MAG" },
                    new VehicleModel {Name = "Viper", Abrv = "VIP" }
                }},
                new VehicleMake {Name = "Ford", Abrv = "FRD", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Mustang", Abrv = "MUST" },
                    new VehicleModel {Name = "GT", Abrv = "GT" },
                    new VehicleModel {Name = "Transit", Abrv = "TRA" },
                    new VehicleModel {Name = "Ranger", Abrv = "RANG" },
                    new VehicleModel {Name = "Super Duty", Abrv = "SD" }
                }},
                new VehicleMake {Name = "Honda", Abrv = "HOND", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Accord", Abrv = "ACC" },
                    new VehicleModel {Name = "Civic", Abrv = "CIV" },
                    new VehicleModel {Name = "Concerto", Abrv = "CON" },
                    new VehicleModel {Name = "Integra", Abrv = "INT" },
                    new VehicleModel {Name = "Jazz", Abrv = "JZZ" }
                }},
                new VehicleMake {Name = "Opel", Abrv = "OPL", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Ampera", Abrv = "AMP" },
                    new VehicleModel {Name = "Calibra", Abrv = "CAL" },
                    new VehicleModel {Name = "Chevette", Abrv = "CHEV" },
                    new VehicleModel {Name = "Corsa A", Abrv = "CA" },
                    new VehicleModel {Name = "Tigra", Abrv = "TIG" }
                }},
                new VehicleMake {Name = "Porsche", Abrv = "PORS", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "Boxter", Abrv = "BOX" },
                    new VehicleModel {Name = "Cayenne", Abrv = "CAY" },
                    new VehicleModel {Name = "Cayman", Abrv = "CAY" },
                    new VehicleModel {Name = "Macan", Abrv = "MAC" },
                    new VehicleModel {Name = "Panamera", Abrv = "PANAM" }
                }},
                new VehicleMake {Name = "Yugo", Abrv = "YUG", VehicleModels = new List<VehicleModel>
                {
                    new VehicleModel {Name = "45", Abrv = "45" },
                    new VehicleModel {Name = "Florida", Abrv = "FLO" },
                    new VehicleModel {Name = "Poly", Abrv = "POL" },
                    new VehicleModel {Name = "Skala", Abrv = "SKA" },
                    new VehicleModel {Name = "Tempo", Abrv = "TEMP" }
                }}
            };
            listVehicleMakes.ForEach(m => _context.VehicleMakes.Add(m));
            #endregion

            _context.SaveChanges();
        }
    }
}
