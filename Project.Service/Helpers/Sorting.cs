﻿using Project.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Project.Service.Enums;

namespace Project.Service.Helpers
{
    public class Sorting : ISorting
    {
        public string Field { get; set; }
        public SortOrderDirection Direction { get; set; }
    }
}
