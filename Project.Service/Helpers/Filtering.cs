﻿using Project.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.Helpers
{
    public class Filtering : IFiltering
    {
        public string SearchTerm { get; set; }
    }
}
