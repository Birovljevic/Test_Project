﻿using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Project.Service.Enums;

namespace Project.Service.Interfaces
{
    public interface IVehicleModelService : IDisposable
    {
        Task<List<VehicleModel>> GetVehicleModelsAsync(IFiltering filter, ISorting sort, IPaging pagination);
        Task<int> GetVehicleModelCountAsync(string searchTerm);
        Task<VehicleModel> GetVehicleModelByIdAsync(Guid vehicleModelId);
        Task<bool> InsertVehicleModelAsync(VehicleModel vehicleModel);
        Task<bool> DeleteVehicleModelAsync(Guid vehicleModelId);
        Task<bool> UpdateVehicleModelAsync(VehicleModel vehicleModel);
        Task<int> SaveAsync();
    }
}
