﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Project.Service.Enums;

namespace Project.Service.Interfaces
{
    public interface ISorting
    {
        string Field { get; set; }
        SortOrderDirection Direction { get; set; }
    }
}
