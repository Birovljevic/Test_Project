﻿using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Service.Interfaces
{
    public interface IVehicleMake
    {
        Guid ID { get; set; }
        string Name { get; set; }
        string Abrv { get; set; }
        ICollection<VehicleModel> VehicleModels { get; set; }
    }
}
