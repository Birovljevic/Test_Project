﻿using Project.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Project.Service.Enums;

namespace Project.Service.Interfaces
{
    public interface IVehicleMakeService : IDisposable
    {
        Task<List<VehicleMake>> GetVehicleMakesAsync(IFiltering filter, ISorting sort, IPaging pagination);
        Task<int> GetVehicleMakeCountAsync(string searchTerm);
        Task<VehicleMake> GetVehicleMakeByIdAsync(Guid vehicleMakeId);
        Task<bool> InsertVehicleMakeAsync(VehicleMake vehicleMake);
        Task<bool> DeleteVehicleMakeAsync(Guid vehicleMakeId);
        Task<bool> UpdateVehicleMakeAsync(VehicleMake vehicleMake);
        Task<int> SaveAsync();
    }
}
